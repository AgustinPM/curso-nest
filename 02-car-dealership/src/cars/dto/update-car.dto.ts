import { IsOptional, IsString, IsUUID, MinLength } from 'class-validator';

export class UpdateCarDto {
  @IsOptional()
  @IsString()
  @IsUUID()
  readonly id?: string;

  @IsOptional()
  @IsString({ message: `The 'brand' field must be a string` })
  readonly brand?: string;

  @IsOptional()
  @IsString()
  @MinLength(3, { message: `The 'model' field must be at least 3 characters` })
  readonly model?: string;
}
