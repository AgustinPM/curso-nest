import { IsString, MinLength } from 'class-validator';

export class CreateCarDto {
  @IsString({ message: `The 'brand' field must be a string` })
  readonly brand: string;

  @IsString()
  @MinLength(3, { message: `The 'model' field must be at least 3 characters` })
  readonly model: string;
}
